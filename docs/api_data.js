define({ "api": [
  {
    "type": "post",
    "url": "/customers/login",
    "title": "",
    "name": "Customer_Login",
    "group": "Customer",
    "permission": [
      {
        "name": "Public"
      }
    ],
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username input from user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password input from user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>State of result</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token use for authenticate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "UserName",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"UserName\": \"user\"\n  \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoidXNlciIsImlhdCI6MTU3MDk0NTY0MCwiZXhwIjoxNTcxMTE4NDQwfQ.f2SYTY7SWclUwEJRzHx9TUqdZ9riNrGFoR2HipwloQ4\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongUsernameOrPassword",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "GenerateTokenError",
            "description": "<p>Something get wrong when jwt create token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"error\": \"Username or Password not match\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"error\": \"Cannot generate token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/customer/customer.controllers.js",
    "groupTitle": "Customer"
  },
  {
    "type": "GET",
    "url": "/api/products/:id",
    "title": "",
    "description": "<p>Get Product by Id</p>",
    "group": "Product",
    "permission": [
      {
        "name": "Public"
      }
    ],
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<ul> <li>ProductId</li> </ul>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product after updated</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "(Success-Response):",
          "content": "HTTP/1.1 201\n {\n  \"ProductId\": \"5b94a3dc-93c2-4fda-8ab2-d1b99ba7b66c\",\n  \"Name\": \"Nokia l20\",\n  \"UnitPrice\": 333,\n  \"Producer\": \"Nokia\",\n  \"Discount\": 10,\n  \"CategoriesId\": \"211.40.54.173\",\n  \"Image1\": \"http://res.cloudinary.com/ntngoc96/image/upload/v1571107776/SalesEcommerce/Products/1571107770244-ip11.jpg.jpg\",\n  \"Image2\": \"http://res.cloudinary.com/ntngoc96/image/upload/v1571107774/SalesEcommerce/Products/1571107770248-iphone8.jpg.jpg\"\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProductNotFound",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 - Bad Request\n {\n  \"error: \": \"Product not found\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/product/product.controllers.js",
    "groupTitle": "Product",
    "name": "GetApiProductsId"
  },
  {
    "type": "PATCH",
    "url": "/api/product",
    "title": "",
    "description": "<p>Update Product</p>",
    "group": "Product",
    "permission": [
      {
        "name": "Private"
      }
    ],
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Product's id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Product's name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "producer",
            "description": "<p>Producer</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "price",
            "description": "<p>Product's price</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "discount",
            "description": "<p>Product's discount</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "imageurl",
            "description": "<p>Cloudinary image url</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoriid",
            "description": "<p>Categori's Id mapping to Categories</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Product after updated</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "(Success-Response):",
          "content": "HTTP/1.1 201\n {\n   \"product\": {\n      \"ProductId\": \"07e1ab55-dc98-4696\",\n      \"Name\": \"iphone7\",\n      \"ListPrice\": \"3000000\",\n      \"Producer\": \"microsoft\",\n      \"Discount\": 2,\n      \"ImageUrl\": \"https://miro.medium.com/max/2044/1*YRFNzFCvu0gdRHWoTOctPw.png\",\n      \"CategoriesId\": \"211.40.54.173\",\n  },\n  \"msg\": \"Product updated successfully\"\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UpdateFailure",
            "description": "<p>Wrong on SQL or Sequelize</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "FindError",
            "description": "<p>Product may update but not found in database</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 - Bad Request\n {\n    \"error\": {\n        \"name\": \"SequelizeDatabaseError\",\n        \"parent\": {\n            \"code\": \"ER_BAD_FIELD_ERROR\",\n            \"errno\": 1054,\n            \"sqlState\": \"42S22\",\n            \"sqlMessage\": \"Unknown column 'ProdusctId' in 'where clause'\",\n            \"sql\": \"UPDATE `Product` SET `Name`=?,`Producer`=?,`Discount`=?,`ImageUrl`=?,`CategoriesId`=? WHERE `ProdusctId` = ?\",\n            \"parameters\": [\n                \"iphone77\",\n                \"microsoft\",\n                2,\n                \"https://miro.medium.com/max/2044/1*YRFNzFCvu0gdRHWoTOctPw.png\",\n                \"211.40.54.173\",\n                \"no\"\n            ]\n        },\n        ....\n }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 - Bad Request\n {\n  msg: \"Cannot find newly updated product\"\n }",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/product/product.controllers.js",
    "groupTitle": "Product",
    "name": "PatchApiProduct"
  },
  {
    "type": "POST",
    "url": "/api/product",
    "title": "",
    "description": "<p>Create Product</p>",
    "group": "Product",
    "permission": [
      {
        "name": "Private"
      }
    ],
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Product's name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "producer",
            "description": "<p>Producer</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "unitprice",
            "description": "<p>Product's price</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "discount",
            "description": "<p>Product's discount</p>"
          },
          {
            "group": "Parameter",
            "type": "Array[Binary]",
            "optional": false,
            "field": "Image",
            "description": "<p>ProductImage</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoriesid",
            "description": "<p>Categories's Id mapping to Categories</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>The newly created product</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "productImage",
            "description": "<p>The newly created productImages</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "(Success-Response):",
          "content": "HTTP/1.1 201\n {\n   msg: \"Product created successfully\"\n    product: {ProductId: \"52e6c5ba-38dd-4d07-8b45-d5605247ac50\", Name: \"Nokia l20\", UnitPrice: 333,…}\n      CategoriesId: \"211.40.54.173\"\n      Discount: 10\n      Name: \"Nokia l20\"\n      Producer: \"Nokia\"\n      ProductId: \"52e6c5ba-38dd-4d07-8b45-d5605247ac50\"\n      UnitPrice: 333\n    productImages: [{,…}, {,…}]\n      0: {,…}\n        ImageUrl: \"http://res.cloudinary.com/ntngoc96/image/upload/v1571110744/SalesEcommerce/Products/1571110741920-ip11.jpg.jpg\"\n        ProductId: \"52e6c5ba-38dd-4d07-8b45-d5605247ac50\"\n        ProductImageId: \"55a3e7f7-caf8-452d-9794-07a258c26b3c\"\n      1: {,…}\n        ImageUrl: \"http://res.cloudinary.com/ntngoc96/image/upload/v1571110743/SalesEcommerce/Products/1571110741924-iphone8.jpg.jpg\"\n        ProductId: \"52e6c5ba-38dd-4d07-8b45-d5605247ac50\"\n        ProductImageId: \"e8a3aa93-7706-49e9-ba1a-727c8540cbbf\"\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CreateFailure",
            "description": "<p>Wrong on SQL or Sequelize</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "FindError",
            "description": "<p>Product may create but not found in database</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MulterError",
            "description": "<p>Config wrong or file not support</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 - Bad Request\n {\n  {\"error: \": {name: \"SequelizeUniqueConstraintError\",…}}\n       \"error: \": {name: \"SequelizeUniqueConstraintError\",…}\n       errors: [{message: \"PRIMARY must be unique\", type: \"unique violation\", path: \"PRIMARY\",…}]\n       fields: {PRIMARY: \"17344bce-60fb-4238-a9fd-af36f9276fe5\"}\n       name: \"SequelizeUniqueConstraintError\"\n       ....\n }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 - Bad Request\n {\n  msg: \"Cannot find newly created product\"\n }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 - Bad Request\n { \n    Unexpected field\n\n    Error: Unexpected field\n }",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/product/product.controllers.js",
    "groupTitle": "Product",
    "name": "PostApiProduct"
  },
  {
    "type": "post",
    "url": "/auth/admin",
    "title": "",
    "name": "UserLogin",
    "group": "User",
    "permission": [
      {
        "name": "Public"
      }
    ],
    "version": "0.0.1",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username input from user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password input from user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>State of result</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token use for authenticate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "UserName",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"UserName\": \"admin\"\n  \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoidXNlciIsImlhdCI6MTU3MDk0NTY0MCwiZXhwIjoxNTcxMTE4NDQwfQ.f2SYTY7SWclUwEJRzHx9TUqdZ9riNrGFoR2HipwloQ4\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WrongUsernameOrPassword",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "GenerateTokenError",
            "description": "<p>Something get wrong when jwt create token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"error\": \"Username or Password not match\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 - Bad Request\n{\n  \"error\": \"Cannot generate token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/auth/auth.controllers.js",
    "groupTitle": "User"
  }
] });
