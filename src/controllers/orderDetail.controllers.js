import httpStatus from 'http-status';
import uuidv4 from 'uuid/v4'

//Model
import { orderDetail as orderDetailModel } from "../models/OrderDetail";
import { order as orderModel } from "../models/Order";


export async function addOrderDetail(req, res) {
  try {

    let { quantity, unitPrice, discount, productId } = req.body;
    const { orderId } = req.params;
    const orderDetailId = uuidv4();

    const order = await orderModel.findOne({
      where: {
        orderId
      }
    });
    if (!order) {
      throw { "msg": "Order not found" }
    }

    const orderDetail = await orderDetailModel.create({
      orderDetailId,
      quantity,
      unitPrice,
      discount,
      productId,
      orderId
    })

    if (!orderDetail) {
      throw { "msg": "Failure to create order detail" }
    }

    res.status(httpStatus.CREATED).json({
      orderDetail,
      msg: "Order Detail create successfully"
    })
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}


export async function updateOrderDetailById(req, res) {
  try {
    const { orderId, detailId: orderDetailId } = req.params;

    //associate model
    orderModel.hasMany(orderDetailModel, {
      foreignKey: {
        fieldName: 'orderId'
      },
    });
    orderDetailModel.belongsTo(orderModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    })

    let order = await orderModel.findOne({
      where: { orderId },
      include: [{
        model: orderDetailModel,
        where: {
          orderDetailId: orderDetailId
        },
      }]
    });

    if (!order) {
      throw { "msg": "Order or Order Detail not found" };
    }

    const { quantity, unitPrice, discount } = req.body;

    const rowAffected = await orderDetailModel.update({
      quantity,
      unitPrice,
      discount
    }, {
      where: {
        orderDetailId
      }
    })

    if (!rowAffected[0]) {
      throw { "msg": "Failure to update order detail" };
    }

    order = await orderModel.findOne({
      where: { orderId },
      include: [{
        model: orderDetailModel,
        where: {
          orderDetailId: orderDetailId
        },
      }]
    });

    res.status(httpStatus.OK).json({
      order,
      msg: "Order detail update successfully"
    })


  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getOrderDetailByIdAndOrderId(req, res) {
  try {

    //associate model
    orderModel.hasMany(orderDetailModel, {
      foreignKey: {
        fieldName: 'orderId'
      },
    });
    orderDetailModel.belongsTo(orderModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    })
    const { orderId, detailId: orderDetailId } = req.params;

    let order = await orderModel.findOne({
      where: { orderId },
      attributes: { exclude: ['isDeteled'] },
      include: [{
        model: orderDetailModel,
        where: {
          orderDetailId: orderDetailId
        },
      }]
    });

    if (!order) {
      throw { "msg": "Order or Order Detail not found" };
    }

    res.status(httpStatus.OK).json(order);

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getAllOrderDetailsByOrderId(req, res) {
  try {
    //associate model
    orderModel.hasMany(orderDetailModel, {
      foreignKey: {
        fieldName: 'orderId'
      },
    });
    orderDetailModel.belongsTo(orderModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    })
    const { orderId } = req.params;

    let order = await orderModel.findOne({
      where: { orderId },
      attributes: { exclude: ['isDeleted'] },
      include: [{
        model: orderDetailModel,
      }]
    });

    if (!order) {
      throw { "msg": "Order not found" };
    }

    res.status(httpStatus.OK).json(order);

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function deleteOrderDetailByIdAndOrderId(req, res) {
  try {
    const { orderId, detailId: orderDetailId } = req.params;

    const rowAffected = await orderDetailModel.destroy({
      where: {
        orderDetailId,
        orderId
      }
    })

    if (!rowAffected) {
      throw { "msg": "Failure to detele order detail" }
    }

    res.status(httpStatus.OK).json({
      "msg": "Order detail deleted successfully"
    });

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}



