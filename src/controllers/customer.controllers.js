import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import promise from 'bluebird';
import bcrypt from 'bcrypt';
import cloudinary from "cloudinary";
import uuidv4 from "uuid/v4";
import sequelize from 'sequelize';

// Model
import { customerAccount as customerAccountModel } from '../models/CustomerAccount';
import { customer as customerModel } from '../models/Customer';
import { address as addressModel } from '../models/Address';
import { order as orderModel } from "../models/Order";
import { orderDetail as orderDetailModel, orderDetail } from "../models/OrderDetail";

//Helper function
import { uploadPhoto } from "../helper/cloudinary.helpers";

promise.promisify(jwt.sign);
promise.promisify(cloudinary.v2.uploader.upload_stream);

const secrectKey = process.env.PRIVATE_KEY || "appcore";


export async function customerSignIn(req, res) {
  try {
    customerAccountModel.hasOne(customerModel, {
      foreignKey: {
        fieldName: 'accountId'
      }
    });
    customerModel.belongsTo(customerAccountModel, {
      foreignKey: {
        fieldName: 'accountId'
      }
    });

    const error = {};
    const accountId = req.body.username.toString();
    const password = req.body.password.toString();

    const user = await customerAccountModel.findOne({
      where: { accountId },
      attributes: ['accountId', 'password', 'email'], include: [{
        model: customerModel,
        attributes: ['customerId'],
      }]
    })



    //check user exist or not
    if (!user) {
      error.wrongAccount = "Username or Password not match";

      res.status(httpStatus.UNAUTHORIZED).json({
        error
      })
    } else {
      //check password
      const isPasswordCorrect = await bcrypt.compare(password, user.password);

      if (!isPasswordCorrect) {
        error.wrongAccount = "Username or Password not match";
        res.status(httpStatus.UNAUTHORIZED).json({
          error
        })
      }

      const token = jwt.sign({
        accountId,
        rule: "customer"
      }, secrectKey, { expiresIn: "1 days" });
      //fail to generate token
      if (!token) {
        throw { "msg": "Cannot generate token" };
      }

      res.cookie('token', `Bearer ${token}`, { httpOnly: false }).status(httpStatus.OK).json({
        userName: accountId,
        customerId: user.dataValues.customer.dataValues.customerId,
        token
      });


    }
  } catch (error) {
    console.log(error);

    res.status(httpStatus.INTERNAL_SERVER_ERROR).json(
      error
    )
  }
}

export async function customerSignUp(req, res) {
  try {

    customerAccountModel.hasOne(customerModel, {
      foreignKey: {
        fieldName: 'accountId'
      }
    });
    customerModel.belongsTo(customerAccountModel, {
      foreignKey: {
        fieldName: 'accountId'
      }
    });


    const { username, ...newUser } = req.body;

    const checkUsernameExist = await customerAccountModel.count({ where: { accountId: username } });

    if (checkUsernameExist > 0) {
      throw { "msg": "username exist" }
    }

    //hash password
    const salt = await bcrypt.genSalt(parseInt(process.env.SALT_ROUND));

    const passwordHashed = await bcrypt.hash(newUser['password'], salt);

    newUser['accountId'] = username;
    newUser['password'] = passwordHashed;
    let customerId = uuidv4();

    //Customer will create at the same time with customer account
    const [customerAccount, customer] = await Promise.all(
      [
        customerAccountModel.create(newUser),
        customerModel.create({ customerId, accountId: username })
      ]);

    const token = jwt.sign({
      username,
      rule: "customer"
    }, secrectKey, { expiresIn: "1 days" });
    //fail to generate token
    if (!token) {
      throw { "msg": "Cannot generate token" };
    }

    res.cookie('token', `Bearer ${token}`, { httpOnly: false }).status(httpStatus.CREATED).json({
      username: customerAccount.accountId,
      customerId,
      msg: "Create account successfully"
    });

  } catch (err) {
    console.log(`catching error: `, err);

    res.status(httpStatus.BAD_REQUEST).json(err)
  }

}

export async function getLimitCustomers(req, res) {
  try {

    customerAccountModel.hasOne(customerModel, {
      foreignKey: {
        fieldName: 'accountId'
      }
    });
    customerModel.belongsTo(customerAccountModel, {
      foreignKey: {
        fieldName: 'accountId'
      }
    });
    const customers = await customerAccountModel.findAll({
      attributes: ['accountId', 'email'],
      include: [customerModel]
    });

    if (customers.length == 0) {
      throw { msg: "No customer found" }
    }

    res.status(httpStatus.OK).json(customers)
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getCustomerById(req, res) {
  try {
    const { id: customerId } = req.params;
    const customer = await customerModel.findOne({
      where: {
        customerId
      },
      attributes: { exclude: ['accountId', 'isBlocked'] }
    });

    if (customer == null) {
      res.status(httpStatus.BAD_REQUEST).json({
        msg: 'Customer not found'
      });
    }

    res.status(httpStatus.OK).json(customer)
  } catch (err) {
    console.log(`catching error: `, err);

    res.status(httpStatus.BAD_REQUEST).json(err)
  }

}

export async function customerUpdateInfo(req, res) {
  try {
    let { id: customerId } = req.params;
    let { name, address, dob, phonenumber, creditCard } = req.body;

    let customer = await customerModel.findOne({ where: { customerId } });
    if (!customer) {
      throw { "msg": "Customer not found" };
    }

    //update database 
    const rowAffected = await customerModel.update({
      name,
      address,
      dob,
      phonenumber,
      creditCard,
    }, {
      where: {
        customerId
      }
    });
    // rowAffected[0] is n row affected.
    if (!rowAffected[0]) {
      throw { "msg": "Cannot update customer info" };
    }

    customer = await customerModel.findOne({ where: { customerId } });

    res.status(httpStatus.OK).json({
      customer,
      msg: "Customer update successfully"
    });

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function customerUpdateAvatar(req, res) {
  try {
    const { id: customerId } = req.params;
    let avatar = req.file;
    if (avatar == undefined) {
      throw { "msg": "Avatar field is required" }
    }
    const customer = await customerModel.findOne({
      where: {
        customerId,
        isBlocked: false
      }
    });

    if (!customer) {
      throw { msg: "Customer not found" };
    }

    //upload or replace new image to Customers folder in cloudinary
    const avatarUpload = await uploadPhoto(avatar, customerId, 'Customers');

    //update database 
    const rowAffected = await customerModel.update({
      avatar: avatarUpload
    }, {
      where: {
        customerId
      }
    });
    // rowAffected[0] is n row affected.
    if (!rowAffected[0]) {
      throw { "msg": "Cannot update customer info" };
    }


    res.status(httpStatus.OK).json({
      avatarUrl: avatarUpload,
      msg: "Avatar update successfully"
    });
  } catch (error) {
    console.log(`error`, error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function addAddress(req, res) {
  try {
    const { id: customerId } = req.params;

    const customer = await customerModel.findOne({ where: { customerId } });
    if (!customer) {
      throw { "msg": "CustomerId not match" };
    }


    let { addressId, address, name, phonenumber } = req.body;
    const checkAddress = await addressModel.findOne({
      where: {
        addressId
      }
    })

    if (checkAddress !== null) {
      res.status(httpStatus.OK).json({
        addressResult: checkAddress,
        msg: "Add address successfully"
      });
    } else {
      addressId = uuidv4();

      const addressResult = await addressModel.create({
        addressId,
        name,
        address,
        phonenumber,
        customerId
      });

      if (!addressResult) {
        throw { "msg": "Failure to create address" };
      }

      res.status(httpStatus.OK).json({
        addressResult,
        msg: "Add address successfully"
      });
    }




  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getAllAddressByCustomerId(req, res) {
  try {
    const { id: customerId } = req.params;

    const customer = await customerModel.findOne({ where: { customerId } });
    if (!customer) {
      throw { "msg": "CustomerId not match" };
    }

    const address = await addressModel.findAll({
      where: { customerId, isDeleted: false },
      attributes: {
        exclude: ['isDeleted']
      }
    });
    if (address.length == 0) {
      throw { "msg": "No address found, create now!! " };
    }

    res.status(httpStatus.OK).json(address);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function updateAddress(req, res) {
  try {
    const { id: customerId, addressId } = req.params;
    const { name, address, phonenumber } = req.body;
    //associate relation
    customerModel.hasMany(addressModel, {
      foreignKey: {
        fieldName: 'customerId'
      }
    })
    addressModel.belongsTo(customerModel, {
      foreignKey: {
        fieldName: 'customerId'
      }
    })

    let customer = await customerModel.findOne({
      where: { customerId },
      include: [{
        model: addressModel,
        where: {
          addressId
        }
      }],
    });

    if (!customer) {
      throw { "msg": "CustomerId or AddressId not match" };
    }

    const rowAffected = await addressModel.update({
      name,
      address,
      phonenumber
    }, {
      where: {
        addressId
      }
    });
    // check update success or not, n: success, 0: fail
    if (!rowAffected[0]) {
      throw { "msg": "Failure to update address" };
    }
    //find and display result
    customer = await customerModel.findOne({
      where: { customerId },
      include: [{
        model: addressModel,
        where: {
          addressId
        }
      }],
      attributes: ['name']
    });

    res.status(httpStatus.OK).json({
      customer,
      msg: "Update address successfully"
    });


  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function deleteAddressById(req, res) {
  try {
    const { id: customerId, addressId } = req.params;

    let rowAffected = await addressModel.update({
      isDeleted: true,
    }, { where: { addressId, customerId } });
    // check update success or not, n: success, 0: fail
    if (!rowAffected[0]) {
      throw { "msg": "Failure to delete or addressId not exist" };
    }

    res.status(httpStatus.OK).json({
      "msg": "Delete address successfully"
    });

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function addOrder(req, res, next) {
  try {
    const orderId = uuidv4();
    let { dateOrder, addressId, customerId } = req.body;
    if (!dateOrder) {
      dateOrder = new Date();
    }
    const order = await orderModel.create({
      orderId,
      dateOrder,
      customerId,
      addressId
    });

    if (!order) {
      throw { "msg": "Failure to create order" };
    }
    res.status(httpStatus.CREATED).json({
      order,
      msg: "Create order successfully"
    })
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getLimitOrders(req, res) {
  try {
    orderModel.hasMany(orderDetailModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    });
    orderDetailModel.belongsTo(orderModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    })

    const { offset, limit } = req.query;

    const orders = await orderModel.findAll({
      attributes: ['orderId', 'paymentId', 'message', 'dateOrder', 'orderStatus',
        'addressId', 'customerId', 'isDeleted'],
      include: {
        model: orderDetailModel,
      },
      order: [
        ['dateOrder', 'DESC']
      ],
    });

    res.status(httpStatus.OK).json(orders.slice(offset, limit))

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function updateOrder(req, res) {
  try {
    const { paymentId, message, orderStatus, isDeleted } = req.body;
    const { orderId } = req.params;
    const rowAffected = await orderModel.update({
      paymentId,
      orderStatus,
      message,
      isDeleted
    }, {
      where: {
        orderId
      }
    });

    if (!rowAffected[0]) {
      throw { "msg": "Failure to update order" };
    }

    const order = await orderModel.findOne({
      where: {
        orderId
      }
    });

    res.status(httpStatus.OK).json({
      order,
      msg: "Update order successfully"
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getLimitOrdersByCustomerId(req, res) {
  try {
    const { id: customerId } = req.params;
    orderModel.hasMany(orderDetailModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    });
    orderDetailModel.belongsTo(orderModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    })

    const { offset, limit } = req.query;

    const orders = await orderModel.findAll({
      where: {
        customerId,
        isDeleted: false
      },
      attributes: ['orderId', 'paymentId', 'message', 'dateOrder', 'orderStatus',
        'addressId', 'customerId'],
      include: {
        model: orderDetailModel,
      },
      order: [
        ['dateOrder', 'DESC']
      ],

    })

    res.status(httpStatus.OK).json(orders.slice(offset, limit))
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getOrderByCustomerIdAndOrderId(req, res) {
  try {
    const { id: customerId, orderId } = req.params;

    orderModel.hasMany(orderDetailModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    });
    orderDetailModel.belongsTo(orderModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    })

    const order = await orderModel.findOne({
      where: {
        customerId, orderId
      },
      attributes: ['orderId', 'paymentId', 'message', 'dateOrder', 'orderStatus',
        'addressId', 'customerId'],
      include: {
        model: orderDetailModel,
      },
    });

    if (!order) {
      throw { "msg": "No order found" };
    }

    res.status(httpStatus.OK).json(order);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function deleteOrderByCustomerIdAndOrderId(req, res) {
  try {
    const { id: customerId, orderId } = req.params;
    const rowAffected = await orderModel.update({
      isDeleted: true
    }, {
      where: {
        customerId, orderId
      }
    });

    if (!rowAffected[0]) {
      throw { "msg": "Failure to delete order" };
    }

    res.status(httpStatus.OK).json({
      "msg": "Order deleted successfully"
    });
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

