import uuidv4 from 'uuid/v4';
import httpStatus from 'http-status';

//Model
import { category as categoryModel } from "../models/Category";

export async function createCategory(req, res) {
  try {
    const categoryId = uuidv4();
    let { name, order } = req.body;
    order = parseInt(order);

    const category = await categoryModel.create({
      categoryId,
      name,
      order
    })

    if (!category) {
      throw { "error": "Create category fail" };
    }

    res.status(httpStatus.CREATED).json({
      category,
      "msg": "Create category successfully"
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function updateCategory(req, res) {
  try {
    const { id: categoryId } = req.params;
    let { name, order } = req.body;
    order = parseInt(order);

    const rowAffected = await categoryModel.update({ name, order }, { where: { categoryId } });
    // rowAffected[0] is n row affected.
    if (!rowAffected[0]) {
      throw { "msg": "Cannot update category" }
    }

    const category = await categoryModel.findOne({
      where: { categoryId },
      attributes: { exclude: ['isDeleted'] }
    });

    res.status(httpStatus.OK).json({
      category,
      "msg": "Category update successfully"
    })
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function deleteCategory(req, res) {
  try {
    const { id: categoryId } = req.params;

    const rowAffected = await categoryModel.update({
      isDeleted: true
    }, { where: { categoryId } });
    // rowAffected[0] is n row affected.
    if (!rowAffected[0]) {
      throw { "msg": "Failure to delete or CategoryId not found" };
    }

    res.status(httpStatus.OK).json({
      "msg": "Category delete successfully"
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getAllCategories(req, res) {
  try {
    const categories = await categoryModel.findAll({
      where: { isDeleted: false },
      attributes: { exclude: ['isDeleted'] },
      order:[
        ['order','asc']
      ]
    });
    //categories will have list of categories
    if (categories.length == 0) {
      throw { "msg": "Query error or categories empty" };
    }
    res.status(httpStatus.OK).json(categories);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getCategoryById(req, res) {
  try {
    const { id: categoryId } = req.params;
    const category = await categoryModel.findOne({
      where: { categoryId, isDeleted: false },
      attributes: { exclude: ['isDeleted'] }
    });

    res.status(httpStatus.OK).json(category);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}