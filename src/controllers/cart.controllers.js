import httpStatus from 'http-status';
import Stripe from "stripe";
import uuidv4 from 'uuid/v4';

//Model
import { product as productModel } from "../models/Product";
import { productImage as productImageModel } from "../models/productImage";
import { order as orderModel } from "../models/Order";
import { orderDetail as orderDetailModel } from "../models/OrderDetail";
import { Cart } from "../models/Cart";

const sequelize = require('../config/database');

const stripe = Stripe(process.env.STRIPE_SECRET_KEY);

export async function addToCart(req, res) {
  try {
    productModel.hasMany(productImageModel,
      {
        foreignKey: {
          fieldName: 'productId'
        }
      });
    productImageModel.belongsTo(productModel,
      {
        foreignKey: {
          fieldName: 'productId'
        }
      });

    const { productId } = req.params;
    //create cart instance
    const cart = new Cart(req.session.cart ? req.session.cart : {});

    const product = await productModel.findOne({
      where: {
        productId,
        isDeleted: false
      }, include: [productImageModel]
    });


    if (!product) {
      throw { "msg": "Product not found" };
    }
    //if wrong exist, put it into cart
    cart.add(product, productId);

    //reassign to session
    req.session.cart = cart;

    res.status(httpStatus.OK).json({
      cart: cart.generateCart(),
      totalPrice: cart.totalPrice,
      totalQuantity: cart.totalQuantity
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function updateCartItemQuantity(req, res) {
  try {
    const { productId } = req.params;
    const { quantity } = req.body;
    const cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.updateCartItem(productId, quantity);

    req.session.cart = cart;

    res.status(httpStatus.OK).json({
      cart: cart.generateCart(),
      totalPrice: cart.totalPrice,
      totalQuantity: cart.totalQuantity
    });

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function deleteFromCart(req, res) {
  try {
    productModel.hasMany(productImageModel,
      {
        foreignKey: {
          fieldName: 'productId'
        }
      });
    productImageModel.belongsTo(productModel,
      {
        foreignKey: {
          fieldName: 'productId'
        }
      });

    const { productId } = req.params;
    //create cart instance
    const cart = new Cart(req.session.cart ? req.session.cart : {});

    const product = await productModel.findOne({
      where: {
        productId,
        isDeleted: false
      }, include: [productImageModel]
    });


    if (!product) {
      throw { "msg": "Product not found" };
    }
    //if wrong exist, put it into cart
    cart.remove(productId);

    //remove product with zero quantity
    cart.removeEmptyItem();

    //reassign to session
    req.session.cart = cart;

    res.status(httpStatus.OK).json({
      cart: cart.generateCart(),
      totalPrice: cart.totalPrice,
      totalQuantity: cart.totalQuantity
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getAllCartItems(req, res) {
  try {
    const cart = new Cart(req.session.cart || {})

    res.status(httpStatus.OK).json({
      cart: cart.generateCart(),
      totalPrice: cart.totalPrice,
      totalQuantity: cart.totalQuantity
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function deleteAllCart(req, res) {
  try {
    req.session.cart = {};

    res.status(httpStatus.OK).json({
      msg: "Delete cart items successfully"
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}


export async function checkoutCart(req, res, next) {
  try {

    const { name, email, amount, description, stripeToken, orderId } = req.body;
    const { cart } = req.session;
    if (!cart) {
      throw { msg: "Empty cart. Add product first" }
    }

    if (amount != cart.totalPrice) {
      throw { msg: "Totalprice does not match, Try again" }
    }
    //create Stripe customer
    const customer = await stripe.customers.create({
      name,
      email,
      source: stripeToken
    });

    //Execute charge
    const charge = await stripe.charges.create({
      amount: Math.round(amount * 100 * 1000) / 1000,
      currency: "usd",
      customer: customer.id,
      description
    });

    const { id: paymentId } = charge;
    const paymentMessage = description;

    const rowAffected = await orderModel.update({
      paymentId,
      orderStatus: true,
      message: paymentMessage
    }, {
      where: {
        orderId
      }
    });

    if (!rowAffected[0]) {
      throw { "msg": "Failure to update order" };
    }

    const order = await orderModel.findOne({
      where: {
        orderId
      }
    });

    //make cart empty
    req.session.cart = {};

    res.status(httpStatus.OK).json({
      order,
      msg: "Payment successfully"
    });
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function confirmCart(req, res) {
  try {
    const orderId = uuidv4();
    let { dateOrder, addressId, customerId } = req.body;
    if (!dateOrder) {
      dateOrder = new Date();
    }
    //get cart from session
    let cart = new Cart(req.session.cart ? req.session.cart : {});
    //generate to array
    cart = cart.generateCart();


    //create transaction to rollback if sequelize get error
    let transaction = await sequelize.transaction();
    //create order
    const order = await orderModel.create({
      orderId,
      dateOrder,
      customerId,
      addressId
    }, { transaction });

    if (!order) {
      throw { "msg": "Failure to create order" };
    }

    let objProduct;
    //process cart array to carts object 
    let carts = cart.map((value, index) => {
      objProduct = {
        orderDetailId: uuidv4(),
        productId: value.product.productId,
        discount: value.product.discount,
        quantity: value.quantity,
        unitPrice: value.product.unitPrice,
        orderId

      }
      return objProduct;
    });

    const resultOrderDetail = await orderDetailModel.bulkCreate(carts, { transaction });

    if (!resultOrderDetail) {
      throw { "msg": "Failure to create order detail" };
    }

    await transaction.commit();

    res.status(httpStatus.CREATED).json({
      order,
      resultOrderDetail,
      msg: "Confirm cart successfully"
    });
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}


export async function continuePaymentUnfinishedOrder(req, res, next) {
  try {
    orderModel.hasMany(orderDetailModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    });
    orderDetailModel.belongsTo(orderModel, {
      foreignKey: {
        fieldName: 'orderId'
      }
    });

    const { name, email, amount, description, stripeToken, orderId } = req.body;
    let order = await orderModel.findOne({
      where: {
        orderId
      },
      attributes: [],
      include: {
        model: orderDetailModel,
        attributes: ['unitPrice', 'discount', 'quantity']
      },
    });
    if (!order) {
      throw { msg: "Order not found" }
    }
    let { orderDetails } = order;
    let orderAmount = 0;

    for (let index = 0; index < orderDetails.length; index++) {
      orderAmount += (orderDetails[index].dataValues.unitPrice - (orderDetails[index].dataValues.unitPrice * orderDetails[index].dataValues.discount) / 100) * orderDetails[index].dataValues.quantity
      orderAmount = Math.round(orderAmount * 1000) / 1000;
    }

    if (amount != orderAmount) {
      throw { msg: "Totalprice does not match, Try again" }
    }
    //create Stripe customer
    const customer = await stripe.customers.create({
      name,
      email,
      source: stripeToken
    });

    //Execute charge
    const charge = await stripe.charges.create({
      amount: Math.round(amount * 100 * 1000) / 1000,
      currency: "usd",
      customer: customer.id,
      description
    });

    const { id: paymentId } = charge;
    const paymentMessage = description;

    const rowAffected = await orderModel.update({
      paymentId,
      orderStatus: true,
      message: paymentMessage
    }, {
      where: {
        orderId
      }
    });

    if (!rowAffected[0]) {
      throw { "msg": "Failure to update order" };
    }

    order = await orderModel.findOne({
      where: {
        orderId
      }
    });

    res.status(httpStatus.OK).json({
      order,
      msg: "Payment successfully"
    });
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}