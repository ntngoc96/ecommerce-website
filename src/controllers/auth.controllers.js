import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import httpStatus from 'http-status';

// Model
import { userAccount } from '../models/User';

const secrectKey = process.env.PRIVATE_KEY || "appcore";

export async function userSignIn(req, res) {
  try {
    const error = {};
    const accountId = req.body.username.toString();
    const password = req.body.password.toString();

    const user = await userAccount.findOne({ where: { accountId }, attributes: ['accountId', 'password', 'email'] });

    //check user exist or not
    if (!user) {
      error.wrongAccount = "Username or Password not match";
      res.status(httpStatus.UNAUTHORIZED).json({
        error
      })
    } else {
      //check password
      const isPasswordCorrect = await bcrypt.compare(password, user.password);

      if (!isPasswordCorrect) {
        error.wrongAccount = "Username or Password not match";
        res.status(httpStatus.UNAUTHORIZED).json({
          error
        })
      }

      //genarate token
      const token = jwt.sign({
        accountId,
        rule: "admin"
      }, secrectKey, { expiresIn: "1 days" });
      //fail to generate token
      if (!token) {
        throw { "msg": "Cannot generate token" };
      }

      res.cookie('token', `Bearer ${token}`, { httpOnly: false }).status(httpStatus.OK).json({
        userName: accountId,
        token
      });


    }
  } catch (error) {
    console.log(error);

    res.status(httpStatus.INTERNAL_SERVER_ERROR).json(
      error
    )
  }
}

