
//Dependencies
import uuidv4 from "uuid/v4";
import httpStatus from 'http-status';
import cloudinary from 'cloudinary';
import promise from "bluebird";
import multer from 'multer';
import Sequelize from 'sequelize';

const sequelize = require('../config/database');

//Model
import { product as productModel } from '../models/Product';
import { productImage as productImageModel } from '../models/ProductImage';

//Helper function
import { uploadPhoto, uploadPhotos } from "../helper/cloudinary.helpers";


promise.promisify(cloudinary.v2.uploader.upload_stream);
promise.promisify(cloudinary.v2.uploader.destroy);

export async function getLimitProducts(req, res) {
  try {
    productModel.hasMany(productImageModel, {
      foreignKey: {
        fieldName: 'productId'
      }
    });
    productImageModel.belongsTo(productModel, {
      foreignKey: {
        fieldName: 'productId'
      }
    });

    //limit, offset, sortBy,type will set default value if query undefined
    const limit = req.query.limit ? req.query.limit : 10;
    const offset = req.query.offset >= 0 ? req.query.offset : 0;
    const sortBy = req.query.by ? req.query.by : 'name';
    const type = req.query.type ? req.query.type : 'DESC';


    const products = await productModel.findAll({
      where: {
        isDeleted: false
      },
      attributes: {
        exclude: ['isDeleted']
      },
      include: [productImageModel],
      order: [
        [sortBy, type]
      ],
      limit: parseInt(limit),
      offset: parseInt(offset)
    })

    if (products.length == 0) {
      throw { "msg": "No product found" };
    }

    res.status(httpStatus.OK).json(products)
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getProductImagesById(req, res) {
  try {
    const { id: productId } = req.params;

    const productImages = await productImageModel.findAll({
      where: { productId },
    });

    if (productImages.length == 0) {
      throw { "msg": "Product not found or does not have image" };
    }

    res.status(httpStatus.OK).json(productImages);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function getProductById(req, res, next) {
  try {
    // associate product and productImage
    productModel.hasMany(productImageModel, {
      foreignKey: {
        fieldName: 'productId'
      }
    });
    productImageModel.belongsTo(productModel, {
      foreignKey: {
        fieldName: 'productId'
      }
    });
    const productId = req.params.id;

    const product = await productModel.findAll({
      where: { productId, isDeleted: false },
      attributes: { exclude: ['isDeleted'] },
      include: [productImageModel],
      plain: true
    });

    // null if product not found
    if (!product) {
      throw { "msg": "Product not found" };
    }

    res.status(httpStatus.OK).json(
      product
    )

  } catch (err) {
    console.log(err);

    res.status(httpStatus.BAD_REQUEST)
      .json({ 'error: ': err })

  }
}

export async function addProduct(req, res) {
  try {

    let transaction = await sequelize.transaction();
    const productId = uuidv4();
    const { name, unitPrice, producer, discount, categoryId } = req.body;
    // insert to product table at the same time with upload to cloudinary
    const product = await productModel.create({
      productId,
      name,
      unitPrice: parseFloat(unitPrice),
      producer,
      discount: parseInt(discount),
      categoryId
    }, { transaction });

    //check product isn't exist or not after create
    if (!product) {

      throw { "msg": "Cannot find newly created product" }
    }

    // Wait util cloudinary uploading done
    let imageUrls = await uploadPhotos(req.files);

    // Create ProductImage records
    const productImages = [];
    imageUrls.forEach(imageUrlObj => {

      productImages.push({
        imageUrl: imageUrlObj.imageUrl,
        productImageId: imageUrlObj.productImageId,
        productId,
      })


    });

    const resultProductImages = await productImageModel.bulkCreate(productImages, { transaction });

    await transaction.commit();

    res.status(httpStatus.CREATED).json({
      product,
      "productImages": resultProductImages,
      "msg": "Product created successfully"
    });

  } catch (err) {
    console.log('catch error: ', err);
    // Check error of image upload 
    if (err instanceof multer.MulterError) {
      //error in multer
      return res.status(httpStatus.BAD_REQUEST).json(err)
    } else if (err) {
      //error config multer
      return res.status(httpStatus.BAD_REQUEST).json(err)
    }

    res.status(httpStatus.BAD_REQUEST)
      .json({ "error: ": err })
  }

}

export async function addProductImages(req, res, next) {
  try {
    const { id: productId } = req.params;

    const product = await productModel.findOne({
      where: { productId }
    });

    //check product isn't exist or not after create
    if (!product) {
      throw { "msg": "Cannot found product" }
    }

    const images = req.files;

    if (images.length == 0) {
      throw { "error": "Product Images is required" };
    }

    // Wait util cloudinary uploading done
    let imageUrls = await uploadPhotos(images);

    // Create ProductImage records
    const productImages = [];
    imageUrls.forEach(imageUrlObj => {

      productImages.push({
        imageUrl: imageUrlObj.imageUrl,
        productImageId: imageUrlObj.productImageId,
        productId,
      })


    });

    const resultProductImages = await productImageModel.bulkCreate(productImages);

    res.status(httpStatus.CREATED).json({
      product,
      "productImages": resultProductImages,
      "msg": "Product Images created successfully"
    });

  } catch (err) {
    console.log('catch error: ', err);
    // Check error of image upload 
    if (err instanceof multer.MulterError) {
      //error in multer
      return res.status(httpStatus.BAD_REQUEST).json(err)
    } else if (err) {
      //error config multer
      return res.status(httpStatus.BAD_REQUEST).json(err)
    }

    res.status(httpStatus.BAD_REQUEST)
      .json({ "error: ": err })
  }

}

export async function updateProductById(req, res, next) {
  try {
    const { id: productId } = req.params;
    const { name, unitPrice, producer, discount, description, star, categoryId } = req.body;

    let rowAffected = await productModel.update({
      name,
      unitPrice,
      producer,
      discount: parseInt(discount),
      description,
      star,
      categoryId
    }, {
      where: {
        productId,
        isDeleted: false
      },
    });

    // check update success or not, n: success, 0: fail
    if (!rowAffected[0]) {
      throw { msg: "Product wasn't updated" };
    }
    let product = await productModel.findOne({ where: { productId } });


    //if product change success but not found with that id, database may has some problem
    if (!product) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
        msg: "Cannot find newly created product"
      })
    }
    res.status(httpStatus.OK).json({
      product,
      msg: "Product updated successfully"
    });


  } catch (err) {
    res.status(httpStatus.BAD_REQUEST)
      .json({ err })
  }
}

export async function deleteProductById(req, res, next) {
  try {
    const { id: productId } = req.params;

    const rowAffected = await productModel.update({
      isDeleted: true,
    }, {
      where: {
        productId
      }
    })
    // check update success or not, n: success, 0: fail
    if (!rowAffected[0]) {
      throw { 'msg': 'Failure to delete or productid not exist' }
    }
    res.status(httpStatus.OK).json({
      'msg': 'Delete successfully'
    })

  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })

  }
}

export async function updateProductImageById(req, res) {
  try {

    const { id: productImageId } = req.params;
    const image = req.file;
    //handle image undefined
    if (!image) {
      throw { "msg": "Product Image is required" };
    }
    //upload or replace to cloudinary
    let imageUrl = await uploadPhoto(image, productImageId, "Products");

    res.status(httpStatus.OK).json({
      imageUrl: imageUrl,
      msg: "Update product image successfully"
    })


  } catch (error) {
    console.log(`error: ${error}`);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function deleteProductImageById(req, res) {
  try {

    const { id: productImageId } = req.params;
    //delete old image on cloudinary
    const deleteResult = await cloudinary.v2.uploader.destroy(`SalesEcommerce/Products/${productImageId}`, { invalidate: true });
    //"not found": no image match with public_id, "ok": deleted
    if (deleteResult.result == 'not found') {
      throw { "msg": "ProductImage not found" };
    }

    const isDeletedInDB = await productImageModel.destroy({ where: { productImageId } });

    if (!isDeletedInDB) {
      throw { "msg": "ProductImage deleted in cloudinary but not deleted in database" };
    }

    res.status(httpStatus.OK).json({
      msg: "Delete product image successfully"
    })


  } catch (error) {
    console.log(`error: ${error}`);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}

export async function filterProductsByColumnName(req, res) {
  try {
    productModel.hasMany(productImageModel, {
      foreignKey: {
        fieldName: 'productId'
      }
    });
    productImageModel.belongsTo(productModel, {
      foreignKey: {
        fieldName: 'productId'
      }
    });


    //Sequelize operators
    const Op = Sequelize.Op;
    const { columnName } = req.params;
    let products = [];
    if (columnName == "name" || columnName == "producer") {
      const { q: name } = req.query;

      products = await productModel.findAll({
        where: {
          [columnName]: {
            [Op.substring]: name
          }, isDeleted: false
        }, attributes: { exclude: ['isDeleted'] },
        include: [productImageModel]
      });

    } else if (columnName == "unitPrice" || columnName == "star") {
      let { gte, lte } = req.query.q;
      gte = parseInt(gte);
      lte = parseInt(lte);
      //Sequelize operators
      const Op = Sequelize.Op;

      products = await productModel.findAll({
        where: {
          unitPrice: {
            [Op.between]: [gte, lte]
          }, isDeleted: false
        }, attributes:
          { exclude: ['isDeleted'] },
        order: [
          ['unitPrice', 'ASC']
        ],
        include: [productImageModel]
      });
    } else {
      throw { msg: "Wrong filter type. Only: name,producer,unitPrice,star " };
    }

    if (products.length == 0) {
      throw { msg: "No product found" };
    }

    res.status(httpStatus.OK).json(products);
  } catch (error) {
    console.log(error);

    res.status(httpStatus.BAD_REQUEST).json({
      'error': error
    })
  }
}
