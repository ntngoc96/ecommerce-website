import { check } from "express-validator";

export const validateOrderCreateInput = [
  check('addressId')
    .not()
    .isEmpty()
    .withMessage('Address Id field is required')
    .bail()
    .isLength({
      min: 1,
      max: 50
    })
    .withMessage('Address Id field must be at least characters 1 and below 50 characters'),

  check('customerId')
    .not()
    .isEmpty()
    .withMessage('Customer Id field is required')
    .bail()
    .isLength({
      min: 1,
      max: 50
    })
    .withMessage('Customer Id field must be at least characters 1 and below 50 characters'),
]