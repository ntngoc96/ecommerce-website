import { check } from "express-validator";

export const validateAddressInput = [

  check('address')
    .not()
    .isEmpty()
    .withMessage('Address field is required')
    .bail()
    .isLength({
      min: 1,
      max: 200
    })
    .withMessage('Address name field must be at least characters 1 and below 200 characters'),

  check('name')
    .not()
    .isEmpty()
    .withMessage('Name field is required')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('Name field must be at least characters 1 and below 200 characters'),

  check('phonenumber')
    .not()
    .isEmpty()
    .withMessage('Phonenumber field is required')
    .bail()
    .isLength({
      min: 1,
      max: 15
    })
    .withMessage('Phonenumber is must be at least characters 1 and below and 15'),

]
