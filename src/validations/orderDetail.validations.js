import { check } from "express-validator";

export const validateOrderDetailCreateInput = [
  check('quantity')
    .not()
    .isEmpty()
    .withMessage('Quantity field is required')
    .bail()
    .isInt({
      min: 1,
      max: 9999
    })
    .withMessage('Quantity field must be between 1 and 9999'),

  check('unitPrice')
    .not()
    .isEmpty()
    .withMessage('Price field is required')
    .bail()
    .isFloat()
    .withMessage('Price field must be type float'),


  check('discount')
    .not()
    .isEmpty()
    .withMessage('Discount field is required')
    .bail()
    .isInt({
      min: 1,
      max: 99  
    })
    .withMessage('Discount field must be between 99'),

  check('productId')
    .not()
    .isEmpty()
    .withMessage('Product Id field is required')
    .bail()
    .isLength({
      min: 1,
      max: 50
    })
    .withMessage('Product Id field must be at least characters 1 and below 50 characters'),
  
]

export const validateOrderDetailUpdateInput = [
  check('quantity')
    .not()
    .isEmpty()
    .withMessage('Quantity field is required')
    .bail()
    .isInt({
      min: 1,
      max: 9999
    })
    .withMessage('Quantity field must be between 1 and 9999'),

  check('unitPrice')
    .not()
    .isEmpty()
    .withMessage('Price field is required')
    .bail()
    .isFloat()
    .withMessage('Price field must be type float'),


  check('discount')
    .not()
    .isEmpty()
    .withMessage('Discount field is required')
    .bail()
    .isInt({
      min: 1,
      max: 99  
    })
    .withMessage('Discount field must be between 99'),

  
]