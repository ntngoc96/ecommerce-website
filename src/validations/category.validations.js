import { check } from 'express-validator';

export const validateCategoryCreateInput = [

  check('name')
    .not()
    .isEmpty()
    .withMessage('Category field is required')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('Category field must be at least characters 1 and below 45 characters'),  
    
  check('order')
    .not()
    .isEmpty()
    .withMessage('Order field is required')
    .bail()
    .isInt({
      min: 1,
      max: 20
    })
    .withMessage('Order is an number between 1 and 20'),  
]

