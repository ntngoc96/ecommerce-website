import { check } from 'express-validator';

export const validateProductUpdateInput = [

  check('name')
    .not()
    .isEmpty()
    .withMessage('Product name field is required')
    .bail()
    .isLength({
      min: 0,
      max: 45
    })
    .withMessage('Product name field must be below 45 characters'),

  check('unitPrice')
    .not()
    .isEmpty()
    .withMessage('Price field is required')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('Price field must be at least 1 characters and below 100 characters'),
  
  check('discount')
    .not()
    .isEmpty()
    .withMessage('Discount field is required')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('Discount field must be at least 1 characters and below 100 characters'),
  
    check('producer')
    .not()
    .isEmpty()
    .withMessage('Producer field is required')
    .bail()
    .isLength({
      min: 1,
      max: 20
    })
    .withMessage('Producer field must be at least 1 characters and below 20 characters'),

  check('description')
    .not()
    .isEmpty()
    .withMessage('Description field is required')
    .bail()
    .isLength({
      min: 1,
      max: 9999
    })
    .withMessage('Description field must be at least 1 characters and below 9999 characters'),

  check('star')
    .not()
    .isEmpty()
    .withMessage('Star field is required')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('Star field must be at least 1 characters and below 5 characters'),

]

export const validateProductCreateInput = [

  check('name')
    .not()
    .isEmpty()
    .withMessage('Product name field is required')
    .bail()
    .isLength({
      min: 0,
      max: 45
    })
    .withMessage('Product name field must be below 45 characters'),

  check('unitPrice')
    .not()
    .isEmpty()
    .withMessage('Price field is required')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('Price field must be at least 1 characters and below 100 characters'),
  
  check('discount')
    .not()
    .isEmpty()
    .withMessage('Discount field is required')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('Discount field must be at least 1 characters and below 100 characters'),
  
    check('producer')
    .not()
    .isEmpty()
    .withMessage('Producer field is required')
    .bail()
    .isLength({
      min: 1,
      max: 20
    })
    .withMessage('Producer field must be at least 1 characters and below 20 characters'),

  check('categoryId')
    .not()
    .isEmpty()
    .withMessage('Category Id field is required')
    .bail()
    .isLength({
      min: 1,
      max: 50
    })
    .withMessage('Star field must be at least 1 characters and below 50 characters'),
]
