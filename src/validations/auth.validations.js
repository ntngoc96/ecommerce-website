import { check } from 'express-validator';

export const validateSignUpInput = [

  check('username')
    .not()
    .isEmpty()
    .withMessage('Username field is required')
    .bail()
    .isLength({
      min: 6,
      max: 50
    })
    .withMessage('Username field must be at least 6 characters and below 50 characters'),

  check('password')
    .not()
    .isEmpty()
    .withMessage('Password field is required')
    .bail()
    .isLength({
      min: 6,
      max: 100
    })
    .withMessage('Password field must be at least 6 characters and below 100 characters'),
    
  check('email')
    .not()
    .isEmpty()
    .withMessage('Email field is required')
    .bail()
    .isEmail()
    .withMessage('Wrong email format'),

]
export const validateSignInInput = [

  check('username')
    .not()
    .isEmpty()
    .withMessage('Username field is required')
    .bail()
    .isLength({
      min: 6,
      max: 50
    })
    .withMessage('Username field must be at least 6 characters and below 50 characters'),

  check('password')
    .not()
    .isEmpty()
    .withMessage('Password field is required')
    .bail()
    .isLength({
      min: 6,
      max: 100
    })
    .withMessage('Password field must be at least 6 characters and below 100 characters')
]