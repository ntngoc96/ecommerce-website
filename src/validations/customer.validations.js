import { check } from "express-validator";

export const validateCustomerInfoUpdateInput = [
  check('name')
    .not()
    .isEmpty()
    .withMessage('Name field is required')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('Name field must be at least characters 1 and below 45 characters'),

  check('address')
    .not()
    .isEmpty()
    .withMessage('Address field is required')
    .bail()
    .isLength({
      min: 1,
      max: 100
    })
    .withMessage('Name field must be at least characters 1 and below 100 characters'),

  check('dob')
    .not()
    .isEmpty()
    .withMessage('Dob field is required')
    .bail()
    .isISO8601()
    .withMessage('Dob format wrong'),

  check('phonenumber')
    .not()
    .isEmpty()
    .withMessage('Phonenumber field is required')
    .bail()
    .isLength({
      min: 1,
      max: 15
    })
    .withMessage('Phonenumber field must be at least characters 1 and below 15 characters'),

  check('creditCard')
    .not()
    .isEmpty()
    .withMessage('Credit card field is required')
    .bail()
    .isLength({
      min: 1,
      max: 45
    })
    .withMessage('Name field must be at least characters 1 and below 45 characters'),
]