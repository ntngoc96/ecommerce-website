const router = require('express').Router();
// const passport = require('passport');

import { userSignIn } from '../../controllers/auth.controllers';

import { validateSignInInput } from '../../validations/auth.validations';
import { handleValidationError } from '../../middlewares/validate.middlewares';

/**
 * @api {post} /api/v1/auth/admin
 * @apiDescription Admin Login
 * @apiGroup Admin
 * @apiPermission Public
 * 
 * @apiParam {String} username
 * @apiParam {String} password
 * 
 * @apiSuccess (OK 200) {Boolean} userName 
 * @apiSuccess (OK 200) {String} token Token use for authenticate
 * 
 * @apiError (Unauthorization 401) {Object} error - Wrong username or password
 * 
 * @apiError (Bad Request 400) {String} msg - Failure to generate jwt token
 * 
 */
router.post(
  '/admin',
  validateSignInInput,
  handleValidationError,
  userSignIn
)


export { router as authRoute }