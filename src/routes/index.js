import express from 'express';

import { authRoute } from './auth/auth.routes';
import { productRoute } from './api/product.routes';
import { customerRoute } from './api/customer.routes';
import { categoryRoute } from './api/category.routes';
import { orderRoute } from './api/order.routes';
import { cartRoute } from "./api/cart.routes";

const router = express.Router();

router.use('/auth/v1', authRoute);
router.use('/api/v1/products', productRoute);
router.use('/api/v1/customers', customerRoute);
router.use('/api/v1/categories', categoryRoute);
router.use('/api/v1/orders', orderRoute);
router.use('/api/v1/carts',cartRoute)

module.exports = router;