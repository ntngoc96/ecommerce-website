import express from "express";

import {
  addToCart,
  deleteFromCart,
  getAllCartItems,
  checkoutCart,
  deleteAllCart,
  updateCartItemQuantity,
  confirmCart,
  continuePaymentUnfinishedOrder,
} from "../../controllers/cart.controllers";

import {
  verifyToken,
} from "../../middlewares/auth.middlewares";
const router = express.Router();

/**
* @api {get} /api/v1/carts/:productId
* @apiDescription Add item to cart
* @apiGroup Cart
* @apiPermission Public
*
* @apiParam {String} productId
* 
* @apiSuccess (OK 200) {Array} cart
*
*/
router.get(
  '/:productId',
  addToCart);




/**
* @api {get} /api/v1/carts
* @apiDescription Get all cart items
* @apiGroup Cart
* @apiPermission Public
*
* @apiSuccess (OK 200) {Array} cart
*
*/
router.get(
  '/',
  getAllCartItems
)


/**
* 
* @api {patch} /api/v1/carts/:productId
* @apiDescription Update quatity of item in cart
* @apiGroup Cart
* @apiPermission Public
*
* @apiParam {String} productId
*
* @apiSuccess (OK 200) {Array} cart
*
*/
router.patch(
  '/:productId',
  updateCartItemQuantity);


/**
* 
* @api {delete} /api/v1/carts/:productId
* @apiDescription Remove cart item
* @apiGroup Cart
* @apiPermission Public
*
* @apiParam {String} productId
*
* @apiSuccess (OK 200) {Array} cart
* 
* @apiError (Bad Request 400) {Object[]} msg - Error message
*
*/
router.delete(
  '/:productId',
  deleteFromCart);


/**
* 
* @api {delete} /api/v1/carts/empty/all
* @apiDescription Remove cart item
* @apiGroup Cart
* @apiPermission Public
*
* @apiSuccess (OK 200) {Object} msg
* 
* @apiError (Bad Request 400) {Object[]} msg - Error message
*
*/
router.delete(
  '/empty/all',
  deleteAllCart);


/**
* 
* @api {post} /api/v1/carts/checkout/confirm
* @apiDescription Confirm Cart and Create Order,OrderDetail
* @apiGroup Cart
* @apiPermission Private
*
* @apiSuccess (OK 200) {Object} order
* @apiSuccess (OK 200) {Object[]} resultOrderDetail
* @apiSuccess (OK 200) {String} msg
* 
* @apiError (Unauthorization 401) {Object[]} msg - Unauthorization
*
* @apiError (Bad Request 400) {Object[]} msg - ErrorMessage
*
*/
router.post(
  '/checkout/confirm',
  verifyToken,
  confirmCart);


/**
* 
* @api {post} /api/v1/carts/checkout/pay
* @apiDescription Process payment and update order status
* @apiGroup Cart
* @apiPermission Private
*
* @apiSuccess (OK 200) {Object[]} order
* @apiSuccess (OK 200) {String} msg
* 
* @apiError (Unauthorization 401) {Object[]} msg - Unauthorization
*
* @apiError (Bad Request 400) {Object[]} msg - ErrorMessage
*
*/
router.post(
  '/checkout/pay',
  verifyToken,
  checkoutCart);



/**
* 
* @api {post} /api/v1/carts/checkout/oldOrder
* @apiDescription Continue to pay unfinished orders
* @apiGroup Cart
* @apiPermission Private
*
* @apiSuccess (OK 200) {Object[]} order
* @apiSuccess (OK 200) {String} msg
* 
* @apiError (Unauthorization 401) {Object[]} msg - Unauthorization
*
* @apiError (Bad Request 400) {Object[]} msg - ErrorMessage
*
*/
router.post(
  '/checkout/oldOrder',
  verifyToken,
  continuePaymentUnfinishedOrder);




export { router as cartRoute }