import express from 'express';

import {
  customerSignIn,
  customerSignUp,
  getCustomerById,
  customerUpdateInfo,
  customerUpdateAvatar,
  addAddress,
  getAllAddressByCustomerId,
  updateAddress,
  deleteAddressById,
  getLimitOrdersByCustomerId,
  getOrderByCustomerIdAndOrderId,
  deleteOrderByCustomerIdAndOrderId,
  getLimitCustomers,
} from '../../controllers/customer.controllers';

import {
  validateSignUpInput,
  validateSignInInput,
} from '../../validations/auth.validations';

import {
  validateAddressInput,
} from '../../validations/address.validations';

import {
  validateCustomerInfoUpdateInput
} from '../../validations/customer.validations';


import {
  verifyToken, verifyAdmin, 
} from "../../middlewares/auth.middlewares";

import { multerUploads } from "../../config/multer.config";
import { handleValidationError } from '../../middlewares/validate.middlewares';

const router = express.Router();


/**
 * @api {get} /api/v1/customers/:id
 * @apiDescription Get specific customer
 * @apiGroup Customer
 * @apiPermission Private
 *
 * @apiParam {String} id customerId
 * 
 * @apiSuccess (OK 200) {String} customerId
 * @apiSuccess (OK 200) {String} name
 * @apiSuccess (OK 200) {String} address
 * @apiSuccess (OK 200) {String} dob
 * @apiSuccess (OK 200) {String} phonenumber
 * @apiSuccess (OK 200) {String} avatar
 * @apiSuccess (OK 200) {String} creditCard
 * 
 * @apiError (Unauthorized 401) {String} success - false
 * @apiError (Unauthorized 401) {Object} msg - Error message
 * 
 * @apiError (Bad Request 400) {String} msg - Customer not found
 *
 */
router.get('/:id',
  verifyToken,
  getCustomerById
);



/**
 * @api {get} /api/v1/customers
 * @apiDescription Get all customers
 * @apiGroup Customer
 * @apiPermission Private
 * 
 * @apiParam {String} offset - query optional 
 * @apiParam {String} limit - query optional 
 * 
 * @apiSuccess (OK 200) {Array} customers
 *
 * @apiError (Unauthorized 401) {String} success - false
 * @apiError (Unauthorized 401) {Object} msg - Error message
 * 
 * @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
 */
router.get('/',
  verifyToken,
  verifyAdmin,
  getLimitCustomers
);


/**
 * @api {post} /api/v1/customers
 * @apiDescription Create customer account
 * @apiGroup Customer
 * @apiPermission Public
 *
 * @apiParam {String} username
 * @apiParam {String} password
 * @apiParam {String} email
 * 
 * @apiSuccess (OK 200) {String} username
 * @apiSuccess (OK 200) {String} customerId
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Bad Request 400) {String} msg - User name exist
 * 
 * @apiError (Bad Request 400) {Object[]} msg - Validate error message
 *
 */
router.post('/',
  validateSignUpInput,
  handleValidationError,
  customerSignUp);


/**
 * @api {post} /api/v1/customers/login
 * @apiDescription Customer Login
 * @apiGroup Customer
 * @apiPermission Public
 * 
 * @apiParam {String} username Username input from user
 * @apiParam {String} password Password input from user
 * 
 * @apiSuccess (OK 200) {Boolean} userName 
 * @apiSuccess (OK 200) {String} customerId
 * @apiSuccess (OK 200) {String} token Token use for authenticate
 * 
 * @apiError (Unauthorization 401) {Object} error - Wrong username or password
 * 
 * @apiError (Bad Request 400) {String} msg - Failure to generate jwt token
 * 
 */
router.post(
  '/login',
  validateSignInInput,
  handleValidationError,
  customerSignIn)


/**
 * @api {patch} /api/v1/customers/:id/personal-info
 * @apiDescription Update customer information
 * @apiGroup Customer
 * @apiPermission Private
 * 
 * @apiParam {String} id customerId
 * 
 * @apiParam {String} username
 * @apiParam {String} address
 * @apiParam {Date} dob
 * @apiParam {String} phonenumber
 * @apiParam {String} creditCard
 * 
 * @apiSuccess (OK 200) {Object} customer - Customer information  
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 *  
 * @apiError (Bad Request 400) {Object} error - Customer not found
 * 
 * @apiError (Bad Request 400) {Array} error - Validate error message
 * 
 */
router.patch(
  '/:id/personal-info',
  verifyToken,
  validateCustomerInfoUpdateInput,
  handleValidationError,
  customerUpdateInfo)


/**
 * @api {patch} /api/v1/customers/:id/avatar
 * @apiDescription Update customer avatar
 * @apiGroup Customer
 * @apiPermission Private
 * 
 * @apiParam {String} id customerId
 * 
 * @apiParam {Binary} avatar 
 * 
 * @apiSuccess (OK 200) {String} imageUrl
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Internal Server Error 500) {HTMLtag} Unexpected field
 * 
 * @apiError (Bad Request 400) {Object} error - Customer not found
 * 
 * @apiError (Bad Request 400) {Object} error - Validate error message
 * 
 * 
 */
router.patch(
  '/:id/avatar',
  verifyToken,
  multerUploads.single('avatar'),
  customerUpdateAvatar)


/**
 * @api {post} /api/v1/customers/:id/address
 * @apiDescription Add customer address for shipping
 * @apiGroup Customer
 * @apiPermission Private
 *
 * @apiParam {String} id customerId
 * 
 * @apiParam {String} name
 * @apiParam {String} address
 * @apiParam {String} addressId
 * @apiParam {String} phonenumber
 * 
 * @apiSuccess (Created 201) {Object[]} addressResult
 * @apiSuccess (Created 201) {String} msg
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {Object} error - CustomerId not match
 * 
 * @apiError (Bad Request 400) {Object[]} msg - Validate error message
 *
 */
router.post('/:id/address',
  verifyToken,
  validateAddressInput,
  handleValidationError,
  addAddress
);


/**
 * @api {get} /api/v1/customers/:id/address
 * @apiDescription Get all shipping address of customer
 * @apiGroup Customer
 * @apiPermission Private
 *
 * @apiParam {String} id customerId
 * 
 * @apiSuccess (OK 200) {String} addressId
 * @apiSuccess (OK 200) {String} name
 * @apiSuccess (OK 200) {String} address
 * @apiSuccess (OK 200) {String} phonenumber
 * @apiSuccess (OK 200) {String} customerId
 * 
 * @apiError (Unauthorized 401) {String} success - false
 * @apiError (Unauthorized 401) {Object} msg - Error message
 * 
 * @apiError (Bad Request 400) {Object} error - CustomerId not match
 *
 * @apiError (Bad Request 400) {Object} error - No address found
 *
 */
router.get('/:id/address',
  verifyToken,
  getAllAddressByCustomerId
);


/**
 * @api {put} /api/v1/customers/:id/address/:addressId
 * @apiDescription Update shipping address of customer
 * @apiGroup Customer
 * @apiPermission Private
 *
 * @apiParam {String} id customerId
 * @apiParam {String} addressId 
 * 
 * @apiParam {String} name
 * @apiParam {String} address
 * @apiParam {String} phonenumber
 * 
 * @apiSuccess (OK 200) {Object} customer
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {Object} error - CustomerId or AddressId not match
 * 
 * @apiError (Bad Request 400) {Object[]} msg - Validate error message
 *
 */
router.put('/:id/address/:addressId',
  verifyToken,
  validateAddressInput,
  handleValidationError,
  updateAddress
);


/**
* 
* @api {delete} /api/v1/customers/:id/address/:addressId
* @apiDescription Delete specific shipping address
* @apiGroup Customer
* @apiPermission Private
*
* @apiParam {String} id customerId
* @apiParam {String} addressId
*
* @apiSuccess (OK 200) {String} msg
* 
* @apiError (Bad Request 400) {Object} error - Error message
*
*/
router.delete('/:id/address/:addressId',
  verifyToken,
  deleteAddressById
);


/**
 * @api {get} /api/v1/customers/:id/orders?offset=:n&limit=:m
 * @apiDescription Get all orders of customer
 * @apiGroup Customer
 * @apiPermission Private
 *
 * @apiParam {String} id customerId
 * @apiParam {String} offset - query optional 
 * @apiParam {String} limit - query optional 
 * 
 * @apiSuccess (OK 200) {Array} orders
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {Object} error - No order found or customerId not match
 * 
 */
router.get('/:id/orders',
  verifyToken,
  getLimitOrdersByCustomerId,
);


/**
 * @api {get} /api/v1/customers/:id/orders/:orderId
 * @apiDescription Get specific order of customer
 * @apiGroup Customer
 * @apiPermission Private
 *
 * @apiParam {String} id customerId
 * @apiParam {String} orderId
 * 
 * @apiSuccess (OK 200) {String} orderId
 * @apiSuccess (OK 200) {String} paymentId
 * @apiSuccess (OK 200) {String} message
 * @apiSuccess (OK 200) {String} dateOrder
 * @apiSuccess (OK 200) {Boolean} orderStatus
 * @apiSuccess (OK 200) {Boolean} isDeleted
 * @apiSuccess (OK 200) {String} customerId
 * @apiSuccess (OK 200) {String} addressId
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {Object} error - No order found
 * 
 */
router.get('/:id/orders/:orderId',
  verifyToken,
  getOrderByCustomerIdAndOrderId,
);


/**
 * @api {delete} /api/v1/customers/:id/orders/:orderId
 * @apiDescription Delete specific order of customer
 * @apiGroup Customer
 * @apiPermission Private
 *
 * @apiParam {String} id customerId
 * @apiParam {String} orderId
 * 
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {Object} error - Failure to delete order
 * 
 */
router.delete('/:id/orders/:orderId',
  verifyToken,
  deleteOrderByCustomerIdAndOrderId,
);


export { router as customerRoute }


