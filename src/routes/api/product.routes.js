import express from 'express';
import {
  getLimitProducts,
  getProductImagesById,
  getProductById,
  deleteProductById,
  deleteProductImageById,
  addProduct,
  addProductImages,
  updateProductById,
  updateProductImageById,
  filterProductsByColumnName,
} from '../../controllers/product.controllers';

import {
  validateProductUpdateInput,
  validateProductCreateInput,
} from '../../validations/product.validations';

import { multerUploads } from '../../config/multer.config';

import {
  verifyToken,
  verifyAdmin,
} from "../../middlewares/auth.middlewares";
import { handleValidationError } from '../../middlewares/validate.middlewares';
const router = express.Router();


/**
 * @api {get} /api/v1/products?offset=:n&limit=:m&by=:columnName&type=[DESC|ACS]
 * @apiDescription Get products
 * @apiGroup Product
 * @apiPermission Public
 *
 * @apiParam {String} offset - query optional 
 * @apiParam {String} limit - query optional 
 * @apiParam {String} by - Product field - query optional 
 * @apiParam {String} type - Sort type - query optional 
 * 
 * @apiSuccess (OK 200) {Array} products
 * 
 * 
 * @apiError (Bad Request 400) {String} msg - No product found
 * 
 */
router.get(
  '/',
  getLimitProducts)


/**
 * @api {get} /api/v1/products/:id/images
 * @apiDescription Get images of product
 * @apiGroup Product
 * @apiPermission Public
 *
 * @apiParam {String} id - customerId 
 * 
 * @apiSuccess (OK 200) {Array} products images
 * 
 * 
 * @apiError (Bad Request 400) {String} msg - Product not found or does not have image
 * 
 */
router.get(
  '/:id/images',
  getProductImagesById)


/**
* @api {GET} /api/v1/products/:id
* @apiDescription Get specific product by id
* @apiGroup Product
* @apiPermission Public
*
* @apiParam {String} id - productId
*
* @apiSuccess (OK 200) {String} productId
* @apiSuccess (OK 200) {String} name
* @apiSuccess (OK 200) {Number} unitPrice
* @apiSuccess (OK 200) {String} producer
* @apiSuccess (OK 200) {Number} discount
* @apiSuccess (OK 200) {String} description
* @apiSuccess (OK 200) {Number} star
* @apiSuccess (OK 200) {String} categoryId
* @apiSuccess (OK 200) {Array} productImages
* 
* @apiError (Bad Request 400) {String} msg - No product found
* 
*/
router.get(
  '/:id',
  getProductById)

/**
* @api {GET} /api/v1/products/:id
* @apiDescription Get filter product
* @apiGroup Product
* @apiPermission Public
*
* @apiParam {String} id - productId
*
* @apiSuccess (OK 200) {Array} products
* 
* @apiError (Bad Request 400) {Object} error - No product found
* 
*/
router.get(
  '/filter/:columnName',
  filterProductsByColumnName
)


/**
* @api {POST} /api/v1/product
* @apiDescription Create Product
* @apiGroup Product
* @apiPermission Private
*
* @apiParam {String} name
* @apiParam {String} producer
* @apiParam {String} unitprice
* @apiParam {Number} discount
* @apiParam {String} categoryid 
* @apiParam {Array[Binary]} files - Product Images
*
* @apiSuccess (Created 201) {Object} product
* @apiSuccess (Created 201) {Array} productImages
* @apiSuccess (Created 201) {String} msg
*
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
* 
* @apiError (Bad Request 400) {Array} errors - Validate Error Message
* 
* @apiError (Bad Request 400) {Object} errors - SequelizeForeignKeyConstraintError
*
*/
router.post(
  '/',
  verifyToken,
  verifyAdmin,
  multerUploads.array('files', 10),
  validateProductCreateInput,
  handleValidationError,
  addProduct)


/**
* @api {POST} /api/v1/product/:id/images
* @apiDescription Create product images
* @apiGroup Product
* @apiPermission Private
*
* @apiParam {String} id - productId
*
* @apiParam {Array[Binary]} files - Product Images
*
* @apiSuccess (Created 201) {Object} product
* @apiSuccess (Created 201) {Array} productImages
* @apiSuccess (Created 201) {String} msg
*
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
* 
* @apiError (Bad Request 400) {Array} errors - Validate Error Message
* 
* @apiError (Bad Request 400) {String} msg - Product not found
*
*/
router.post(
  '/:id/images',
  verifyToken,
  verifyAdmin,
  multerUploads.array('files', 10), //limit 10 images once
  addProductImages)


/**
* @api {patch} /api/products/:id
* @apiDescription Update specific product
* @apiGroup Product
* @apiPermission Private
*
* @apiParam {String} name 
* @apiParam {Number} unitPrice 
* @apiParam {String} producer
* @apiParam {Number} discount
* @apiParam {Number} description
* @apiParam {Number} star
* @apiParam {String} categoryId
*
* @apiSuccess (OK 200) {Object} product
* @apiSuccess (OK 200) {String} msg
*
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
* 
* @apiError (Bad Request 400) {Array} errors - Validate Error Message
* 
* @apiError (Bad Request 400) {Object} error - Product was not update
*
*/
router.patch(
  '/:id',
  verifyToken,
  verifyAdmin,
  validateProductUpdateInput,
  handleValidationError,
  updateProductById)





/**
* @api {delete} /api/products/:id
* @apiDescription Delete specific product
* @apiGroup Product
* @apiPermission Private
*
* @apiParam {String} id - productId
*
* @apiSuccess (OK 200) {String} msg
*
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
*
* @apiError (Bad Request 400) {Object} error - Failure to delete or productid not exist
*
*/
router.delete(
  '/:id',
  verifyToken,
  verifyAdmin,
  deleteProductById)


/**
* @api {delete} /api/products/images/:id
* @apiDescription Delete specific product images
* @apiGroup Product
* @apiPermission Private
*
* @apiParam {String} id - productImageId
*
* @apiSuccess (OK 200) {String} msg
*
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
*
* @apiError (Bad Request 400) {Object} error - Product image not found
*
* @apiError (Bad Request 400) {Object} error - ProductImage deleted in cloudinary but not deleted in database
*/
router.delete(
  '/images/:id',
  verifyToken,
  verifyAdmin,
  deleteProductImageById)


/**
* @api {patch} /api/products/images/:id
* @apiDescription Update specific product image
* @apiGroup Product
* @apiPermission Private
*
* @apiParam {String} name 
* @apiParam {Number} unitPrice 
* @apiParam {String} producer
* @apiParam {Number} discount
* @apiParam {Number} description
* @apiParam {Number} star
* @apiParam {String} categoryId
*
* @apiSuccess (OK 200) {String} imageUrl
* @apiSuccess (OK 200) {String} msg
*
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
* 
* @apiError (Bad Request 400) {Object} errors - Validate Error Message
* 
* @apiError (Bad Request 400) {Object} error - Product image not found
* 
*
*/
router.patch(
  '/images/:id',
  verifyToken,
  verifyAdmin,
  multerUploads.single('file'),
  updateProductImageById)


export { router as productRoute }




