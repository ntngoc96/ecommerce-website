import express from 'express';

import {
  addOrderDetail,
  updateOrderDetailById,
  getOrderDetailByIdAndOrderId,
  getAllOrderDetailsByOrderId,
  deleteOrderDetailByIdAndOrderId,
} from '../../controllers/orderDetail.controllers';

import { getLimitOrders, addOrder, updateOrder } from '../../controllers/customer.controllers';

import {
  validateOrderDetailCreateInput,
  validateOrderDetailUpdateInput,
} from '../../validations/orderDetail.validations';


import {
  validateOrderCreateInput,
} from '../../validations/order.validations';

import { verifyToken, verifyAdmin } from "../../middlewares/auth.middlewares";
import { handleValidationError } from '../../middlewares/validate.middlewares';
const router = express.Router();


/**
 * @api {get} /api/v1/orders?offset=:n&limit=:m
 * @apiDescription Get orders
 * @apiGroup Admin
 * @apiPermission Private
 *
 * @apiParam {String} offset - query optional 
 * @apiParam {String} limit - query optional 
 * 
 * @apiSuccess (OK 200) {Array} orders
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
 * 
 */
router.get(
  '/',
  verifyToken,
  verifyAdmin,
  getLimitOrders
)


/**
* @api {post} /api/v1/orders
* @apiDescription Add new customer's order
* @apiGroup Admin
* @apiPermission Private
* 
* @apiParam {String} orderId
*
* @apiParam {Number} quantity
* @apiParam {Number} unitPrice
* @apiParam {Number} discount
* @apiParam {String} productId
* 
* @apiSuccess (Created 201) {Object} order
* @apiSuccess (Created 201) {String} msg
* 
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
* 
* @apiError (Bad Request 400) {Array} error - Validate Error Message
* 
* @apiError (Bad Request 400) {Object} error - Constrain SQL error
* 
*/
router.post('/',
  verifyToken,
  verifyAdmin,
  validateOrderCreateInput,
  handleValidationError,
  addOrder,
);


/**
* @api {post} /api/v1/orders/:orderId/details
* @apiDescription Add new order detail to order
* @apiGroup Admin
* @apiPermission Private
* 
* @apiParam {String} orderId
*
* @apiParam {Number} quantity
* @apiParam {Number} unitPrice
* @apiParam {Number} discount
* @apiParam {String} productId
* 
* @apiSuccess (Created 201) {Object} orderDetail
* @apiSuccess (Created 201) {String} msg
* 
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
* 
* @apiError (Bad Request 400) {Array} error - Validate Error Message
* 
* @apiError (Bad Request 400) {String} msg - Order not found
* 
*/
router.post('/:orderId/details',
  verifyToken,
  verifyAdmin,
  validateOrderDetailCreateInput,
  handleValidationError,
  addOrderDetail
)


/**
* @api {patch} /api/v1/orders/:orderId
* @apiDescription Update order
* @apiGroup Admin
* @apiPermission Private
* 
* @apiParam {String} orderId
*
* @apiParam {String} paymentId
* @apiParam {String} message
* @apiParam {Boolean} orderStatus
* @apiParam {Boolean} isDeleted
* 
* @apiSuccess (Created 201) {Object} order
* @apiSuccess (Created 201) {String} msg
* 
* @apiError (Unauthorization 401) {String} success - false
* @apiError (Unauthorization 401) {String} error
* 
* @apiError (Unauthorization 401) {String} msg - Admin Role Requirement
* 
* @apiError (Bad Request 400) {String} msg - Failure to update order
* 
*/
router.patch('/:orderId',
  verifyToken,
  verifyAdmin,
  updateOrder
)


/**
 * @api {patch} /api/v1/orders/:orderId/details/:detailId
 * @apiDescription Update order detail
 * @apiGroup Order
 * @apiPermission Private
 *
 * @apiParam {String} orderId
 * @apiParam {String} detailId - orderDetailId
 * 
 * @apiSuccess (OK 200) {Object} order
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {String} msg - Order or Order Detail not found
 * 
 * @apiError (Bad Request 400) {String} msg - Validate Error Message
 * 
 */
router.patch('/:orderId/details/:detailId',
  verifyToken,
  validateOrderDetailUpdateInput,
  handleValidationError,
  updateOrderDetailById
)


/**
 * @api {get} /api/v1/orders/:orderId/details/:detailId
 * @apiDescription Get specific order and order detail
 * @apiGroup Order
 * @apiPermission Private
 *
 * @apiParam {String} orderId
 * @apiParam {String} detailId - orderDetailId
 * 
 * @apiSuccess (OK 200) {String} orderId
 * @apiSuccess (OK 200) {String} paymentId
 * @apiSuccess (OK 200) {String} message
 * @apiSuccess (OK 200) {String} dateOrder
 * @apiSuccess (OK 200) {Boolean} orderStatus
 * @apiSuccess (OK 200) {Boolean} isDeleted
 * @apiSuccess (OK 200) {String} customerId
 * @apiSuccess (OK 200) {String} addressId
 * @apiSuccess (OK 200) {Object} orderDetails
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {String} msg - Order or Order Detail not found
 * 
 */
router.get('/:orderId/details/:detailId',
  verifyToken,
  getOrderDetailByIdAndOrderId,
)


/**
 * @api {get} /api/v1/orders/:orderId/details
 * @apiDescription Get all order detail of order
 * @apiGroup Order
 * @apiPermission Private
 *
 * @apiParam {String} orderId
 * 
 * @apiSuccess (OK 200) {String} orderId
 * @apiSuccess (OK 200) {String} paymentId
 * @apiSuccess (OK 200) {String} message
 * @apiSuccess (OK 200) {String} dateOrder
 * @apiSuccess (OK 200) {Boolean} orderStatus
 * @apiSuccess (OK 200) {Boolean} isDeleted
 * @apiSuccess (OK 200) {String} customerId
 * @apiSuccess (OK 200) {String} addressId
 * @apiSuccess (OK 200) {Object[]} orderDetails
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {String} msg - Order not found
 * 
 */
router.get('/:orderId/details',
  verifyToken,
  getAllOrderDetailsByOrderId,
)


/**
 * @api {delete} /api/v1/orders/:orderId/details/:detailId
 * @apiDescription Delete specific order detail with orderId
 * @apiGroup Order
 * @apiPermission Private
 *
 * @apiParam {String} orderId
 * @apiParam {String} detailId - orderDetailId
 * 
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Unauthorization 401) {String} success - false
 * @apiError (Unauthorization 401) {String} error
 * 
 * @apiError (Bad Request 400) {String} msg - Failure to detele order detail
 * 
 */
router.delete('/:orderId/details/:detailId',
  verifyToken,
  deleteOrderDetailByIdAndOrderId,
);


export { router as orderRoute };



