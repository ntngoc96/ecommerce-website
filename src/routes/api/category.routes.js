import express from 'express';

import {
  getAllCategories,
  createCategory,
  updateCategory,
  deleteCategory,
  getCategoryById,
} from '../../controllers/category.controllers';

import {
  validateCategoryCreateInput,
} from '../../validations/category.validations';

import {
  verifyToken,
  verifyAdmin,
} from "../../middlewares/auth.middlewares";
import { handleValidationError } from '../../middlewares/validate.middlewares';

const router = express.Router();


/**
 * @api {get} /api/v1/categories
 * @apiDescription Get all categories
 * @apiGroup Category
 * @apiPermission Public
 *
 * @apiSuccess (OK 200) {Array} categories
 *
 */
/* GET get all categories */
router.get('/',
  getAllCategories
)


/**
 * @api {get} /api/v1/categories/:id
 * @apiDescription Get specific category
 * @apiGroup Category
 * @apiPermission Public
 *
 * @apiParam {String} id Category Id
 * 
 * @apiSuccess (OK 200) {Object[]} categoryId
 * @apiSuccess (OK 200) {Object[]} name
 * @apiSuccess (OK 200) {Object[]} order
 *
 */
router.get('/:id',
  getCategoryById
)


/**
 * @api {post} /api/v1/categories/:id
 * @apiDescription Create category
 * @apiGroup Category
 * @apiPermission Private
 *
 * @apiParam {String} id Category Id
 * 
 * @apiParam {String} name Category Name
 * @apiParam {Number} order Category Name
 * 
 * @apiSuccess (OK 201) {Object[]} categoryId
 * @apiSuccess (OK 201) {Object[]} name
 * @apiSuccess (OK 201) {Object[]} order
 * 
 * @apiError (Bad Request 400) {String} msg - Validate Error Message
 * 
 * @apiError (Unauthorized 401) {Boolean} success
 * @apiError (Unauthorized 401) {Object[]} error - Error Message
 *
 */
router.post('/',
  verifyToken,
  verifyAdmin,
  validateCategoryCreateInput,
  handleValidationError,
  createCategory
)


/**
 * @api {patch} /api/v1/categories/:id
 * @apiDescription Update category
 * @apiGroup Category
 * @apiPermission Private
 *
 * @apiParam {String} id Category Id
 * 
 * @apiParam {String} name Category Name
 * @apiParam {Number} order Category Name
 * 
 * @apiSuccess (OK 200) {Object[]} category
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Bad Request 400) {String} msg - Validate Error Message
 * 
 * @apiError (Unauthorized 401) {Boolean} success
 * @apiError (Unauthorized 401) {Object[]} error - Error Message
 *
 */
router.patch('/:id',
  verifyToken,
  verifyAdmin,
  validateCategoryCreateInput,
  handleValidationError,
  updateCategory
)


/**
 * @api {delete} /api/v1/categories/:id
 * @apiDescription Delete category
 * @apiGroup Category
 * @apiPermission Private
 *
 * @apiParam {String} id Category Id
 * 
 * @apiSuccess (OK 200) {String} msg
 * 
 * @apiError (Unauthorized 401) {Boolean} success
 * @apiError (Unauthorized 401) {Object[]} error - Error Message
 *
 */
router.delete('/:id',
  verifyToken,
  verifyAdmin,
  deleteCategory
)

export { router as categoryRoute };