import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';

//Model
const secrectKey = process.env.PRIVATE_KEY;

export function getToken(req, res, next) {
  const error = {};
  // get auth header value
  const bearerHeader = req.headers['authorization'];

  // check if bearer is undefined
  if (typeof bearerHeader !== 'undefined') {
    //split header at the space
    const bearer = bearerHeader.split(' ');
    //get token
    const token = bearer[1];

    if (!token) {
      error.invalidToken = "Auth token invalid";
      res.status(httpStatus.UNAUTHORIZED).json({
        success: false,
        error
      })
    }

    req.token = token;
    next();
  } else {
    error.invalidToken = "Auth token invalid";
    res.status(httpStatus.UNAUTHORIZED).json({
      success: false,
      error
    })
  }
}


export function verifyToken(req, res, next) {
  const error = {};
  // get auth header value
  let bearerHeader = req.headers['authorization'] || req.headers['x-access-token'] || req.cookies.token;;

  // check if bearer is undefined
  if (typeof bearerHeader !== 'undefined') {
    bearerHeader = bearerHeader.replace('%20', ' ');
    //split header at the space
    const bearer = bearerHeader.split(' ');
    //get token
    const token = bearer[1];
    console.log(bearerHeader);

    if (!token) {

      error.invalidToken = "Auth token invalid";
      res.status(httpStatus.UNAUTHORIZED).json({
        success: false,
        error
      })
    }

    jwt.verify(token, secrectKey, (err, decoded) => {
      if (err) {
        error.invalidToken = "Auth token invalid";
        res.status(httpStatus.UNAUTHORIZED).json({
          success: false,
          error
        })
      }

      req.user = decoded;
      next();

    })
  } else {
    error.invalidToken = "Please provide token";
    res.status(httpStatus.UNAUTHORIZED).json({
      success: false,
      error
    })
  }
}

export function verifyAdmin(req, res, next) {
  console.log(req.user);

  if (req.user.rule == 'admin') {
    next();
  } else {
    res.status(httpStatus.UNAUTHORIZED).json({
      "msg": "Invalid account"
    });
  }



}