
import cloudinary from "cloudinary";
import uuidv4 from "uuid/v4";

export function uploadPhoto(photo, id, folder) {
  return new Promise((resolve, reject) => {
    cloudinary.v2.uploader
      .upload_stream({ public_id: id, folder: `SalesEcommerce/${folder}`, tags: 'customer' }, (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve(result.url);
        }
      })
      .end(photo.buffer);
  });
}

export function uploadPhotos(photos) {
  let productImageId;
  let publicId, temp;
  return new Promise((resolve, reject) => {
    let uploadPromises = photos.map(element => new Promise((resolve, reject) => {
      productImageId = uuidv4();
      // For documentation refer the following link
      // https://cloudinary.com/documentation/node_image_upload
      cloudinary.v2.uploader
        .upload_stream({ public_id: productImageId, folder: 'SalesEcommerce/Products', tags: 'product' }, (error, result) => {
          if (error) {
            reject(error);
          } else {
            //public_id example: SalesEcommerce/Products/48871856-07af-48c8-8a9d-74cc0617f389
            temp = result.public_id.split('/');
            publicId = temp[2];
            resolve({ imageUrl: result.url, productImageId: publicId });
          }
        })
        .end(element.buffer);
    }));
    //waiting for all images upload done
    Promise.all(uploadPromises)
      .then(result => resolve(result))
      .catch(err => reject(err))
  })
}