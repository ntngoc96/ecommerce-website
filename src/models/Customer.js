import { DataTypes } from 'sequelize';
import db from '../config/database';

export const customer = db.define("customer", {
  customerId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING
  },
  address: {
    type: DataTypes.STRING
  },
  dob: {
    type: DataTypes.DATE
  },
  phonenumber: {
    type: DataTypes.STRING
  },
  avatar: {
    type: DataTypes.STRING
  },
  creditCard: {
    type: DataTypes.STRING
  },
  isBlocked: {
    type: DataTypes.TINYINT
  },
  accountId: {
    type: DataTypes.STRING
  },
}, {
  timestamps: false,
  freezeTableName: true,
})

