import { DataTypes } from 'sequelize';
import db from '../config/database';

export const product = db.define("product", {
  productId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING
  },
  unitPrice: {
    type: DataTypes.STRING
  },
  producer: {
    type: DataTypes.STRING
  },
  discount: {
    type: DataTypes.INTEGER
  },
  description: {
    type: DataTypes.STRING
  },
  star: {
    type: DataTypes.INTEGER
  },
  isDeleted: {
    type: DataTypes.TINYINT
  },
  categoryId: {
    type: DataTypes.STRING
  },
}, {
  timestamps: false,
  freezeTableName: true,
})

