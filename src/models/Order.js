import { DataTypes } from 'sequelize';
import db from '../config/database';

export const order = db.define("order", {
  orderId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  paymentId: {
    type: DataTypes.STRING
  },
  message: {
    type: DataTypes.STRING
  },
  dateOrder: {
    type: DataTypes.DATE
  },
  orderStatus: {
    type: DataTypes.STRING
  },
  isDeleted: {
    type: DataTypes.TINYINT
  },
  customerId: {
    type: DataTypes.STRING
  },
  addressId: {
    type: DataTypes.STRING
  },
}, {
  timestamps: false,
  freezeTableName: true,
})

