import { DataTypes } from 'sequelize';
import db from '../config/database';

export const productImage = db.define("productImage",{
  productImageId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  productId: {
    type: DataTypes.STRING
  },
  imageUrl: {
    type: DataTypes.STRING
  },
},{
  timestamps:false,
  freezeTableName: true,
}) 

