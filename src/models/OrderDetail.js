import { DataTypes } from 'sequelize';
import db from '../config/database';

export const orderDetail = db.define("orderDetail", {
  orderDetailId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  quantity: {
    type: DataTypes.INTEGER
  },
  unitPrice: {
    type: DataTypes.FLOAT
  },
  discount: {
    type: DataTypes.INTEGER
  },
  productId: {
    type: DataTypes.STRING
  },
  orderId: {
    type: DataTypes.STRING
  },
}, {
  timestamps: false,
  freezeTableName: true,
})

