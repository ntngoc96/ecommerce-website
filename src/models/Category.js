import { DataTypes } from 'sequelize';
import db from '../config/database';

export const category = db.define("category",{
  categoryId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING
  },
  order: {
    type: DataTypes.INTEGER
  },
  isDeleted: {
    type: DataTypes.TINYINT
  }
},{
  timestamps:false,
  freezeTableName: true,
}) 
