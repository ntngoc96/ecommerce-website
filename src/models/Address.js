import { DataTypes } from 'sequelize';
import db from '../config/database';

export const address = db.define("address", {
  addressId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING
  },
  address: {
    type: DataTypes.INTEGER
  },
  phonenumber: {
    type: DataTypes.STRING
  },
  isDeleted: {
    type: DataTypes.TINYINT
  },
  customerId: {
    type: DataTypes.STRING
  },
}, {
  timestamps: false,
  freezeTableName: true,
}) 
