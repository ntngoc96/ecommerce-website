import { DataTypes } from 'sequelize';
import db from '../config/database';

export const userAccount = db.define("userAccount",{
  accountId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  password: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
},{
  timestamps:false,
  freezeTableName: true,
}) 
