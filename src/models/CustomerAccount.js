import { DataTypes } from 'sequelize';
import db from '../config/database';

export const customerAccount = db.define("customerAccount", {
  accountId: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  password: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
  resetToken: {
    type: DataTypes.STRING
  },
  resetTokenExpire: {
    type: DataTypes.DATE
  },
}, {
  timestamps: false,
  freezeTableName: true,
})
