# sales ecommerce system requirement
## User
* User can registration, login, logout
* User can read, update, delete their information
* User can read, update, delete their products on cart

## Admin
* Admin can CRUD categori and product
* Admin can read user's information
* Admin can edit information contact
* Admin can add or edit payment method

## Other
* Categori has many products
* Product belong to one categories
* Product might have one, many or no gift

## Order 
* Total price gain some quota also have a gift
