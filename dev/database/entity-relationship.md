1.  Entity identification
  * User: User Id (UserID), Name, Dob, Address,Phonenumber, Creadit card, Avatar.
  * UserAccount: Account Id (AccountId), Password, Email, Token, TokenExpire, CanCreate, CanEdit, CanDelete.
  * Customer: Customer Id (CustomerId), Name, Dob, Hobbies, Address, Phonenumber,Credit card, Avatar.
  * CustomerAccount: Account Id (AccountId), Password, Email, Token, TokenExpire.
  * Categori: Categori Id (CategoriId), Name, Order.
  * Product: Product Id (ProductId), Name, Price, Type
  * ProductDetail: Product Detail Id (ProductDetailId), Description, Star, Discount, Image.
  * Gift: Gift Id(GiftId), Name, Price, Description, Image. 
  * Order: Order Id(OrderId), Payment Method, Message.
  * OrderDetail: Order Detail Id (OrderDetailId), Date, Product, Quantity.
  * Shipping: Shipping Id (Shipping Id), Address, Name, Status, Phonenumber, Feedback.

2. Primary key identification
  * User: User Id (UserID)
  * Customer: Customer Id (CustomerId)
  * UserAccount: Account Id (AccountId)
  * CustomerAccount: Account Id (AccountId)
  * Categori: Categori Id (CategoriId)
  * Product: Product Id (ProductId)
  * ProductDetail: Product Detail Id (ProductDetailId)
  * Gift: Gift Id(GiftId)
  * Order: Order Id(OrderId)
  * OrderDetail: Order Detail Id (OrderDetailId)